﻿using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Common.Api.Attributes
{
	public class CategoryAttribute : SwaggerOperationAttribute
	{
		public CategoryAttribute(string category) : base(category)
		{
			Tags = new[] { category };
		}
	}
}