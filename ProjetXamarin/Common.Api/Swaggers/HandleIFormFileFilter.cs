﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Operation = Swashbuckle.Swagger.Model.Operation;

namespace Common.Api.Swaggers
{
	public class HandleIFormFileFilter : IOperationFilter
	{
		public void Apply(Operation operation, OperationFilterContext context)
		{
			for (int i = 0; i < context.ApiDescription.ParameterDescriptions.Count; i++)
			{
				ApiParameterDescription parameterDescription = context.ApiDescription.ParameterDescriptions[i];
				if (parameterDescription.Type == typeof(IFormFile))
				{
					if (operation.Parameters[i] is Swashbuckle.Swagger.Model.NonBodyParameter parameter)
					{
						parameter.In = "formData";
						parameter.Type = "file";
						parameter.Format = "binary";
					}
				}
			}
		}

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            throw new System.NotImplementedException();
        }
    }
}