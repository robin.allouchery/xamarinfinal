﻿using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.Swagger.Model;

namespace Common.Api.Swaggers
{
    public class SortByNameFilter : IDocumentFilter
    {
		public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
		{
			swaggerDoc.Paths = swaggerDoc.Paths.OrderBy(x => x.Key).ToList().ToDictionary(e => e.Key, e => e.Value);
		}

        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            throw new System.NotImplementedException();
        }
    }
}