﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace ProjetXamarin.models
{
    public class RestService
    {
        HttpClient client;
        public RestService()
        {
            client = new HttpClient();
        }

        public async Task PostPlace(PlaceImage place)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places", string.Empty));
            var json = JsonConvert.SerializeObject(place);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
            }
            else
            {
                Debug.WriteLine(response.StatusCode.ToString() + response.RequestMessage);
            }
        }



        public async Task PostCommentaire(int id, string comment)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places/" + id + "/comments", string.Empty));
            string json = "{\n\"text\":\"" + comment + "\"\n}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<SimplePlace> GetPlaces(int id)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places/" + id, string.Empty));
            var reponse = await client.GetAsync(uri);
            SimplePlace res = null;

            if (reponse.IsSuccessStatusCode)
            {
                var content = await reponse.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ClientResponse<SimplePlace>>(content).data;
            }
            return res;
        }

         public async Task<List<Place>> GetPlaces()
        {
            var uri = new Uri(string.Format("https://td-api.julienmialon.com/places", string.Empty));

            var reponse = await client.GetAsync(uri);

            List<Place> res = new List<Place>();

            if (reponse.IsSuccessStatusCode)
            {
                var content = await reponse.Content.ReadAsStringAsync();
                foreach(PlaceImage place in JsonConvert.DeserializeObject<ClientResponse<List<PlaceImage>>>(content).data)
                {
                    res.Add(new Place(place.id,place.title,place.description, "https://td-api.julienmialon.com/images/" + place.image_id,place.latitude,place.longitude));
                }
            }
            return res;
        }

        public async Task<List<string>> GetAllImages()
        {
            List<string> lesImages = new List<string>();
            bool thereIsMoreImage = true;
            int imageId = 1;
            while (thereIsMoreImage)
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://td-api.julienmialon.com/images/" + imageId);
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    lesImages.Add("https://td-api.julienmialon.com/images/" + imageId);
                    imageId++;
                }
                else
                {
                    Debug.WriteLine(response.StatusCode);
                    thereIsMoreImage = false;
                }
            }
            return lesImages;
        }

        public async Task<bool> getImageById(string imageID)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, imageID);
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<int> PostImage(byte[] imageData) 
        { 

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://td-api.julienmialon.com/images");

            MultipartFormDataContent requestContent = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(imageData);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

            requestContent.Add(imageContent, "file", "file.jpg");

            request.Content = requestContent;

            HttpResponseMessage response = await client.SendAsync(request);

            string result = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("img up");
                ClientResponse<UploadedImage> myObject = JsonConvert.DeserializeObject<ClientResponse<UploadedImage>>(result);
                return myObject.data.id;
            }
            return -1;
        }

        public async Task<bool> connexion(string email, string password)
        {
            var uri = new Uri(string.Format("https://td-api.julienmialon.com/auth/login", string.Empty));
            var json = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
    }
}
