﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Permissions;
using ProjetXamarin.models;
using ProjetXamarin.views;
using Storm.Mvvm.Services;
using Xamarin.Forms;
using MvvmNavigationService = Storm.Mvvm.Services.INavigationService;

namespace ProjetXamarin.vms
{
    public class NavigationService : INavigationService
    {
        MvvmNavigationService _service;

        public NavigationService()
        {
           _service= DependencyService.Get<MvvmNavigationService>();
        }

        public async void NavigateBack()
        {
            await _service.PopAsync();
        }

        public async void NavigateToCommentairePage()
        {
            await _service.PushAsync<CommentairePage>();
        }

        public async void NavigateToCommentairePageParam(Dictionary<string, object> dico)
        {
            await _service.PushAsync<CommentairePage>(dico);
        }

        public void NavigateToCommentairePageParam()
        {
            throw new NotImplementedException();
        }

        public async void NavigateToConnexionPage(Dictionary<string, object> dico)
        {
            await _service.PushAsync<ConnexionPage>(dico);
        }


        public async void NavigateToDetailPageParam(Dictionary<string,object> dico, Place item,RestService Trs)
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location);

            if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Location);
                if (results.ContainsKey(Plugin.Permissions.Abstractions.Permission.Location))
                {
                    status = results[Plugin.Permissions.Abstractions.Permission.Location];
                }
            }
            
        }

        public async void NavigateToLieuPage(Dictionary<string, object> dico)
        {
            await _service.PushAsync<LieuPage>(dico);
        }

        public async void NavigateToMainPage()
        {
            await _service.PushAsync<MainPage>();
        }

        private Page GetCurrentPage()
        {
            var currentPage = Application.Current.MainPage;
            return currentPage;
        }

    }
}
