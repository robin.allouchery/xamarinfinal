﻿using ProjetXamarin.models;
using System;
using System.Collections.Generic;

namespace ProjetXamarin.vms
{
    public interface INavigationService
    {
        void NavigateBack();

        void NavigateToLieuPage(Dictionary<string, object> dico);

        void NavigateToConnexionPage(Dictionary<string, object> dico);
        
        void NavigateToDetailPageParam(Dictionary<string, object> dico,Place item,RestService Trs);

        void NavigateToCommentairePage();

        void NavigateToCommentairePageParam();

        void NavigateToMainPage();
    }
}
