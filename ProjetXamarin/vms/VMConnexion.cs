﻿using ProjetXamarin.models;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ProjetXamarin.vms
{
    public class VMConnexion : ViewModelBase
    {
        private RestService _trs;
        [NavigationParameter("restService")]
        public RestService Trs
        {
            get => _trs;
            set => SetProperty(ref _trs, value);
        }

        private VMMain _mainVM;
        [NavigationParameter("MainVM")]
        public VMMain MainVM
        {
            get => _mainVM;
            set => SetProperty(ref _mainVM, value);
        }
        public Command ConnexionCommand { get; private set; }
        public Command CreateAccountCommand { get; private set; }

        private bool _notGood = false;
        public bool NotGood
        {
            get => _notGood;
            set => SetProperty(ref _notGood, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public VMConnexion()
        {


        }
    }
}
