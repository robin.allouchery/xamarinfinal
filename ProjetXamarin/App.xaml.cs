﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetXamarin
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }
    }
}
