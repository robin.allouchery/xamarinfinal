﻿using ProjetXamarin.vms;
using System;
using System.Collections.Generic;
using Storm.Mvvm.Forms;

using Xamarin.Forms;

namespace ProjetXamarin.views
{
    public partial class LieuPage : BaseContentPage
    {
        public LieuPage()
        {
            InitializeComponent();
            BindingContext = new VMLieu();
        }
    }
}
