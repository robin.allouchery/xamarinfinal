﻿using ProjetXamarin.vms;
using System;
using System.Collections.Generic;
using Storm.Mvvm.Forms;
using Xamarin.Forms;
using System.Diagnostics;
using ProjetXamarin.models;

namespace ProjetXamarin.views
{
    public partial class DetailPage : TabbedPage
    {
        
        public DetailPage(Place item,RestService Trs)
        {
            InitializeComponent();
            BindingContext = new VMDetail();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var test = (VMDetail)this.BindingContext;
            test.GetSimplePlace();
        }
    }
}
