﻿using ProjetXamarin.vms;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ProjetXamarin.views
{
    public partial class CommentairePage : ContentPage
    {
        public CommentairePage()
        {
            InitializeComponent();
            BindingContext = new VMCommentaire();
        }
    }
}
