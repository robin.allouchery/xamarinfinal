﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetXamarin.models;
using ProjetXamarin.vms;
using ProjetXamarin.views;
using Storm.Mvvm.Forms;
using Xamarin.Forms;

namespace ProjetXamarin
{
    public partial class MainPage : BaseContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new VMMain();
            list.RefreshCommand = new Command(() =>
            {
                Refreshlist();
            });
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem;
        }

        private void Refreshlist()
        {
            var vmmain = (VMMain)BindingContext;
            vmmain.RefreshList();
            list.IsRefreshing = false;
        }
    }
}
