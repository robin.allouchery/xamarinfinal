package mono;

import java.io.*;
import java.lang.String;
import java.util.Locale;
import java.util.HashSet;
import java.util.zip.*;
import java.util.Arrays;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.util.Log;
import mono.android.Runtime;

public class MonoPackageManager {

	static Object lock = new Object ();
	static boolean initialized;

	static android.content.Context Context;

	public static void LoadApplication (Context context, ApplicationInfo runtimePackage, String[] apks)
	{
		synchronized (lock) {
			if (context instanceof android.app.Application) {
				Context = context;
			}
			if (!initialized) {
				android.content.IntentFilter timezoneChangedFilter  = new android.content.IntentFilter (
						android.content.Intent.ACTION_TIMEZONE_CHANGED
				);
				context.registerReceiver (new mono.android.app.NotifyTimeZoneChanges (), timezoneChangedFilter);

				Locale locale       = Locale.getDefault ();
				String language     = locale.getLanguage () + "-" + locale.getCountry ();
				String filesDir     = context.getFilesDir ().getAbsolutePath ();
				String cacheDir     = context.getCacheDir ().getAbsolutePath ();
				String dataDir      = getNativeLibraryPath (context);
				ClassLoader loader  = context.getClassLoader ();
				java.io.File external0 = android.os.Environment.getExternalStorageDirectory ();
				String externalDir = new java.io.File (
							external0,
							"Android/data/" + context.getPackageName () + "/files/.__override__").getAbsolutePath ();
				String externalLegacyDir = new java.io.File (
							external0,
							"../legacy/Android/data/" + context.getPackageName () + "/files/.__override__").getAbsolutePath ();

				System.loadLibrary("monodroid");

				Runtime.init (
						language,
						apks,
						getNativeLibraryPath (runtimePackage),
						new String[]{
							filesDir,
							cacheDir,
							dataDir,
						},
						loader,
						new String[] {
							externalDir,
							externalLegacyDir
						},
						MonoPackageManager_Resources.Assemblies,
						context.getPackageName (),
						android.os.Build.VERSION.SDK_INT,
						mono.android.app.XamarinAndroidEnvironmentVariables.Variables);
				
				mono.android.app.ApplicationRegistration.registerApplications ();
				
				initialized = true;
			}
		}
	}

	public static void setContext (Context context)
	{
		// Ignore; vestigial
	}

	static String getNativeLibraryPath (Context context)
	{
	    return getNativeLibraryPath (context.getApplicationInfo ());
	}

	static String getNativeLibraryPath (ApplicationInfo ainfo)
	{
		if (android.os.Build.VERSION.SDK_INT >= 9)
			return ainfo.nativeLibraryDir;
		return ainfo.dataDir + "/lib";
	}

	public static String[] getAssemblies ()
	{
		return MonoPackageManager_Resources.Assemblies;
	}

	public static String[] getDependencies ()
	{
		return MonoPackageManager_Resources.Dependencies;
	}

	public static String getApiPackageName ()
	{
		return MonoPackageManager_Resources.ApiPackageName;
	}
}

class MonoPackageManager_Resources {
	public static final String[] Assemblies = new String[]{
		/* We need to ensure that "ProjetXamarin.Android.dll" comes first in this list. */
		"ProjetXamarin.Android.dll",
		"Akavache.Core.dll",
		"Akavache.dll",
		"Akavache.Sqlite3.dll",
		"FCP.Swashbuckle.AspNetCore.SwaggerUI.Versioning.dll",
		"FormsViewGroup.dll",
		"Google.Protobuf.dll",
		"Microsoft.AspNetCore.Antiforgery.dll",
		"Microsoft.AspNetCore.Authentication.Abstractions.dll",
		"Microsoft.AspNetCore.Authentication.Core.dll",
		"Microsoft.AspNetCore.Authorization.dll",
		"Microsoft.AspNetCore.Authorization.Policy.dll",
		"Microsoft.AspNetCore.Connections.Abstractions.dll",
		"Microsoft.AspNetCore.Cors.dll",
		"Microsoft.AspNetCore.Cryptography.Internal.dll",
		"Microsoft.AspNetCore.Cryptography.KeyDerivation.dll",
		"Microsoft.AspNetCore.DataProtection.Abstractions.dll",
		"Microsoft.AspNetCore.DataProtection.dll",
		"Microsoft.AspNetCore.Diagnostics.Abstractions.dll",
		"Microsoft.AspNetCore.Diagnostics.dll",
		"Microsoft.AspNetCore.dll",
		"Microsoft.AspNetCore.HostFiltering.dll",
		"Microsoft.AspNetCore.Hosting.Abstractions.dll",
		"Microsoft.AspNetCore.Hosting.dll",
		"Microsoft.AspNetCore.Hosting.Server.Abstractions.dll",
		"Microsoft.AspNetCore.Html.Abstractions.dll",
		"Microsoft.AspNetCore.Http.Abstractions.dll",
		"Microsoft.AspNetCore.Http.dll",
		"Microsoft.AspNetCore.Http.Extensions.dll",
		"Microsoft.AspNetCore.Http.Features.dll",
		"Microsoft.AspNetCore.HttpOverrides.dll",
		"Microsoft.AspNetCore.HttpsPolicy.dll",
		"Microsoft.AspNetCore.JsonPatch.dll",
		"Microsoft.AspNetCore.Localization.dll",
		"Microsoft.AspNetCore.Mvc.Abstractions.dll",
		"Microsoft.AspNetCore.Mvc.ApiExplorer.dll",
		"Microsoft.AspNetCore.Mvc.Core.dll",
		"Microsoft.AspNetCore.Mvc.Cors.dll",
		"Microsoft.AspNetCore.Mvc.DataAnnotations.dll",
		"Microsoft.AspNetCore.Mvc.dll",
		"Microsoft.AspNetCore.Mvc.Formatters.Json.dll",
		"Microsoft.AspNetCore.Mvc.Localization.dll",
		"Microsoft.AspNetCore.Mvc.Razor.dll",
		"Microsoft.AspNetCore.Mvc.Razor.Extensions.dll",
		"Microsoft.AspNetCore.Mvc.RazorPages.dll",
		"Microsoft.AspNetCore.Mvc.TagHelpers.dll",
		"Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer.dll",
		"Microsoft.AspNetCore.Mvc.Versioning.dll",
		"Microsoft.AspNetCore.Mvc.ViewFeatures.dll",
		"Microsoft.AspNetCore.Razor.dll",
		"Microsoft.AspNetCore.Razor.Language.dll",
		"Microsoft.AspNetCore.Razor.Runtime.dll",
		"Microsoft.AspNetCore.ResponseCaching.Abstractions.dll",
		"Microsoft.AspNetCore.Routing.Abstractions.dll",
		"Microsoft.AspNetCore.Routing.dll",
		"Microsoft.AspNetCore.Server.IIS.dll",
		"Microsoft.AspNetCore.Server.IISIntegration.dll",
		"Microsoft.AspNetCore.Server.Kestrel.Core.dll",
		"Microsoft.AspNetCore.Server.Kestrel.dll",
		"Microsoft.AspNetCore.Server.Kestrel.Https.dll",
		"Microsoft.AspNetCore.Server.Kestrel.Transport.Abstractions.dll",
		"Microsoft.AspNetCore.Server.Kestrel.Transport.Sockets.dll",
		"Microsoft.AspNetCore.StaticFiles.dll",
		"Microsoft.AspNetCore.WebUtilities.dll",
		"Microsoft.CodeAnalysis.CSharp.dll",
		"Microsoft.CodeAnalysis.dll",
		"Microsoft.CodeAnalysis.Razor.dll",
		"Microsoft.Data.Sqlite.dll",
		"Microsoft.DotNet.PlatformAbstractions.dll",
		"Microsoft.Extensions.Caching.Abstractions.dll",
		"Microsoft.Extensions.Caching.Memory.dll",
		"Microsoft.Extensions.Configuration.Abstractions.dll",
		"Microsoft.Extensions.Configuration.Binder.dll",
		"Microsoft.Extensions.Configuration.CommandLine.dll",
		"Microsoft.Extensions.Configuration.dll",
		"Microsoft.Extensions.Configuration.EnvironmentVariables.dll",
		"Microsoft.Extensions.Configuration.FileExtensions.dll",
		"Microsoft.Extensions.Configuration.Json.dll",
		"Microsoft.Extensions.Configuration.UserSecrets.dll",
		"Microsoft.Extensions.DependencyInjection.Abstractions.dll",
		"Microsoft.Extensions.DependencyInjection.dll",
		"Microsoft.Extensions.DependencyModel.dll",
		"Microsoft.Extensions.FileProviders.Abstractions.dll",
		"Microsoft.Extensions.FileProviders.Composite.dll",
		"Microsoft.Extensions.FileProviders.Embedded.dll",
		"Microsoft.Extensions.FileProviders.Physical.dll",
		"Microsoft.Extensions.FileSystemGlobbing.dll",
		"Microsoft.Extensions.Hosting.Abstractions.dll",
		"Microsoft.Extensions.Localization.Abstractions.dll",
		"Microsoft.Extensions.Localization.dll",
		"Microsoft.Extensions.Logging.Abstractions.dll",
		"Microsoft.Extensions.Logging.Configuration.dll",
		"Microsoft.Extensions.Logging.Console.dll",
		"Microsoft.Extensions.Logging.Debug.dll",
		"Microsoft.Extensions.Logging.dll",
		"Microsoft.Extensions.Logging.EventSource.dll",
		"Microsoft.Extensions.ObjectPool.dll",
		"Microsoft.Extensions.Options.ConfigurationExtensions.dll",
		"Microsoft.Extensions.Options.dll",
		"Microsoft.Extensions.Primitives.dll",
		"Microsoft.Extensions.WebEncoders.dll",
		"Microsoft.Net.Http.Headers.dll",
		"Microsoft.OpenApi.dll",
		"Microsoft.Win32.Registry.dll",
		"MySql.Data.dll",
		"Newtonsoft.Json.Bson.dll",
		"Newtonsoft.Json.dll",
		"Plugin.Connectivity.Abstractions.dll",
		"Plugin.Connectivity.dll",
		"Plugin.CurrentActivity.dll",
		"Plugin.Geolocator.dll",
		"Plugin.Media.dll",
		"Plugin.Permissions.dll",
		"ProjetXamarin.dll",
		"ServiceStack.Client.dll",
		"ServiceStack.Common.dll",
		"ServiceStack.dll",
		"ServiceStack.Interfaces.dll",
		"ServiceStack.OrmLite.dll",
		"ServiceStack.OrmLite.MySql.dll",
		"ServiceStack.OrmLite.Sqlite.dll",
		"ServiceStack.Text.dll",
		"SixLabors.Core.dll",
		"SixLabors.ImageSharp.dll",
		"Splat.dll",
		"SQLitePCLRaw.batteries_e_sqlite3.dll",
		"SQLitePCLRaw.batteries_green.dll",
		"SQLitePCLRaw.batteries_v2.dll",
		"SQLitePCLRaw.core.dll",
		"SQLitePCLRaw.lib.e_sqlite3.dll",
		"SQLitePCLRaw.provider.e_sqlite3.dll",
		"Storm.Mvvm.Forms.dll",
		"Swashbuckle.AspNetCore.Annotations.dll",
		"Swashbuckle.AspNetCore.dll",
		"Swashbuckle.AspNetCore.Examples.dll",
		"SwashBuckle.AspNetCore.MicrosoftExtensions.dll",
		"Swashbuckle.AspNetCore.Swagger.dll",
		"Swashbuckle.AspNetCore.SwaggerGen.dll",
		"Swashbuckle.AspNetCore.SwaggerUI.dll",
		"Swashbuckle.Swagger.Master.dll",
		"Swashbuckle.SwaggerGen.Master.dll",
		"Swashbuckle.SwaggerUi.Master.dll",
		"System.Collections.Immutable.dll",
		"System.Configuration.ConfigurationManager.dll",
		"System.Diagnostics.DiagnosticSource.dll",
		"System.IO.Pipelines.dll",
		"System.Net.Http.Formatting.dll",
		"System.Reactive.dll",
		"System.Reflection.Metadata.dll",
		"System.Runtime.CompilerServices.Unsafe.dll",
		"System.Security.AccessControl.dll",
		"System.Security.Cryptography.Xml.dll",
		"System.Security.Permissions.dll",
		"System.Security.Principal.Windows.dll",
		"System.Text.Encodings.Web.dll",
		"System.ValueTuple.dll",
		"Xamarin.Android.Arch.Core.Common.dll",
		"Xamarin.Android.Arch.Lifecycle.Common.dll",
		"Xamarin.Android.Arch.Lifecycle.Runtime.dll",
		"Xamarin.Android.Support.Animated.Vector.Drawable.dll",
		"Xamarin.Android.Support.Annotations.dll",
		"Xamarin.Android.Support.Compat.dll",
		"Xamarin.Android.Support.Core.UI.dll",
		"Xamarin.Android.Support.Core.Utils.dll",
		"Xamarin.Android.Support.CustomTabs.dll",
		"Xamarin.Android.Support.Design.dll",
		"Xamarin.Android.Support.Fragment.dll",
		"Xamarin.Android.Support.Media.Compat.dll",
		"Xamarin.Android.Support.Transition.dll",
		"Xamarin.Android.Support.v4.dll",
		"Xamarin.Android.Support.v7.AppCompat.dll",
		"Xamarin.Android.Support.v7.CardView.dll",
		"Xamarin.Android.Support.v7.MediaRouter.dll",
		"Xamarin.Android.Support.v7.Palette.dll",
		"Xamarin.Android.Support.v7.RecyclerView.dll",
		"Xamarin.Android.Support.Vector.Drawable.dll",
		"Xamarin.Essentials.dll",
		"Xamarin.Forms.Core.dll",
		"Xamarin.Forms.Maps.Android.dll",
		"Xamarin.Forms.Maps.dll",
		"Xamarin.Forms.Platform.Android.dll",
		"Xamarin.Forms.Platform.dll",
		"Xamarin.Forms.Xaml.dll",
		"Xamarin.GooglePlayServices.Base.dll",
		"Xamarin.GooglePlayServices.Basement.dll",
		"Xamarin.GooglePlayServices.Maps.dll",
		"Xamarin.GooglePlayServices.Tasks.dll",
	};
	public static final String[] Dependencies = new String[]{
	};
	public static final String ApiPackageName = "Mono.Android.Platform.ApiLevel_27";
}
